﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuickTutorial.DTOs;
using QuickTutorial.Models;
using QuickTutorial.Repositories.Interfaces;

namespace QuickTutorial.Controllers
{
    // GET api/students
    // Post api/students
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentRepository _studentRepository;

        public StudentsController(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        [HttpGet]
        public IQueryable<Student> GetStudents()
        {
            return _studentRepository.GetStudents();
        }

        [HttpPost]
        public ActionResult AddStudent(StudentDto s)
        {
            Student st = new Student() {
                Id = Guid.NewGuid(),
                Name = s.Name,
                Surname = s.Surname,
                Birthday = s.Birthday,
                GroupId = s.GroupId
            };
            if (_studentRepository.AddStudent(st))
            {
                return Ok("A new student was added successfully!");
            }

            return BadRequest("Oops, something went wrong!");
        }
    }
}
