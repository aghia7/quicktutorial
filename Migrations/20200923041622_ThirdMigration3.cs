﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuickTutorial.Migrations
{
    public partial class ThirdMigration3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "name",
                table: "Groups",
                newName: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Groups",
                newName: "name");
        }
    }
}
