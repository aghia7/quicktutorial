﻿using Microsoft.EntityFrameworkCore;
using QuickTutorial.Data;
using QuickTutorial.Models;
using QuickTutorial.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickTutorial.Repositories
{
    public class StudentRepository : IStudentRepository
    {
        private readonly DataContext _dbContext;

        public StudentRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }
        public bool AddStudent(Student s)
        {
            _dbContext.Students.Add(s);
            return _dbContext.SaveChanges() > 0;
        }

        public IQueryable<Student> GetStudents()
        {
            return _dbContext.Students.Include(x => x.Group).OrderBy(x => x.Name);
        }
    }
}
