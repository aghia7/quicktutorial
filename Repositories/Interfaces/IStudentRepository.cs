﻿using QuickTutorial.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickTutorial.Repositories.Interfaces
{
    public interface IStudentRepository
    {
        IQueryable<Student> GetStudents();
        bool AddStudent(Student s);
    }
}
